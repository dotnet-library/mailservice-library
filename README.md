To use mail service

1) Add dll file for mail service

2) add SmtpSettings option in appsettings.json file like below
  "SmtpSettings": {
    //gmail
    //"Server": "smtp.gmail.com",
    //"Port": 587,
    //"SenderEmail": "abcd@gmail.com",
    //"Password": "123456"

    // hotmail
    //"Server": "smtp.live.com",
    //"Port": 587,
    //"SenderEmail": "abcd@gmail.com",
    //"Password": "123456"

    // office 365
    "Server": "smtp.office365.com",
    "Port": 587,
    //"SenderEmail": "abcd@gmail.com",
    //"Password": "123456"
  }
 
 3) add service in startup page like below
     public class Startup
    {
        private readonly IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            //Email Notification
            services.Configure<SmtpSettings>(Configuration.GetSection("SmtpSettings"));
            services.AddSingleton<IMailer, Mailer>();
        }
	}
	
----------------End--------------------------